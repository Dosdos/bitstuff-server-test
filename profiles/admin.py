from django.contrib import admin
from django.contrib.auth.models import User

from profiles.models import UserProfile


class MyUserAdmin(admin.ModelAdmin):
    list_display = ['pk', 'profile', 'email', 'first_name', 'last_name', 'is_staff']
    list_filter = ['is_staff']
    list_display_links = ['pk']
    list_select_related = True
    search_fields = ['email', 'first_name', 'last_name']


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['pk', 'ghost', 'hunter', 'user', 'born', 'city', 'sex']


#admin.site.unregister(User)
#admin.site.register(User, MyUserAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
