__author__ = 'danielepoggi'

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Fix cashbook writings'
    can_import_settings = True

    def add_arguments(self, parser):
        parser.add_argument('username', nargs='+', type='string')

        # Named (optional) arguments
        parser.add_option('--username',
                          action='store_true',
                          dest='username',
                          default=True,
                          help='Username of USERPROFILE with cashbook writings to fix')


    def handle(self, *args, **options):
        username = args[0]
        u = User(username=username)
        u.set_password(username)
        u.is_superuser = True
        u.is_staff = True
        u.save()



