import json
from smtplib import SMTPException

from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.http.response import HttpResponse
from django.template import loader
from django.template.context import Context
from django.utils.translation import ugettext_lazy
from rest_framework import generics
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated

from bitstuff.settings import DEFAULT_FROM_EMAIL
from profiles.models import UserProfile, generate_random_string
from profiles.serializers import UserProfileSerializer, UserRegisterSerializer, UserUpdateSerializer


"""
PASSWORD MANAGEMENT
"""

@api_view(['GET'])
def password_forgot_request(request):
    profile = request.user.profile
    profile.password_forgot_code = generate_random_string()
    profile.save()
    #
    subject = ugettext_lazy("password forgot code: {0}".format(profile.password_forgot_code))
    #object = DEFAULT_FROM_EMAIL
    emails = [request.user.email]
    #
    t = loader.get_template('profiles/password_forgot_email.html')
    c = Context({
        'user': request.user,
        'code': profile.password_forgot_code,
    })
    try:
        send_mail(subject, "", DEFAULT_FROM_EMAIL, emails, fail_silently=False)
    except SMTPException as e:
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "password_forgot send mail exception"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    # success
    response_data = {'errorCode': 0, 'errorMessage': ""}
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@api_view(['POST'])
def password_forgot_change(request):
    # get password forgot code
    if not 'code' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "POST code not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    code = request.DATA['code']
    # get new password
    if not 'password' in request.DATA:
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "POST password not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    password = request.DATA['password']
    #
    # verify the profile associated with this user has the correct code
    user = request.user
    if not code == user.profile.password_forgot_code:
        # failure
        response_data = {'errorCode': 3, 'errorMessage': "the password forgot code is not correct"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    # change password
    user.set_password(password)
    user.save()
    # change code
    profile = user.profile
    profile.password_forgot_code = generate_random_string()
    profile.save()
    # success
    response_data = {'errorCode': 0, 'errorMessage': ""}
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@api_view(['POST'])
def password_change(request):
    # get password forgot code
    if not 'old_password' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "POST old_password not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    old_password = request.DATA['old_password']
    # get new password
    if not 'new_password' in request.DATA:
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "POST new_password not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    new_password = request.DATA['new_password']
    #
    # verify the profile associated with this user has the correct code
    user = request.user
    if not user.check_password(old_password):
        # failure
        response_data = {'errorCode': 3, 'errorMessage': "the old password does not match"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    # change password
    user.set_password(new_password)
    user.save()
    # success
    response_data = {'errorCode': 0, 'errorMessage': ""}
    return HttpResponse(json.dumps(response_data), content_type="application/json")


"""
USER PROFILE MANAGEMENT
"""


@permission_classes((IsAuthenticated,))
class UserProfileView(generics.RetrieveAPIView):
    model = UserProfile
    serializer_class = UserProfileSerializer

    def get_object(self, queryset=None):
        return UserProfile.objects.get(user=self.request.user)


@api_view(['POST', 'PUT', 'PATCH'])
@permission_classes((IsAuthenticated,))
def user_register(request):
    # first verify data with serializer
    if request.method == 'PUT' or request.method == 'PATCH':
        user_serializer = UserUpdateSerializer(data=request.DATA)
    else:
        user_serializer = UserRegisterSerializer(data=request.DATA)
    profile_serializer = UserProfileSerializer(data=request.DATA)

    if not user_serializer.is_valid():
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "user_serializer errors: %s" % user_serializer.errors}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    if not profile_serializer.is_valid():
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "profile_serializer errors: %s" % profile_serializer.errors}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    # get user with token
    user = request.user
    # update user info
    user_object = user_serializer.object
    user.first_name = user_object.first_name
    user.last_name = user_object.last_name
    if user_object.username:
        user.username = user_object.username
    if user_object.email:
        user.email = user_object.email
    if user_object.password:
        user.set_password(user_object.password)
    user.save()
    # update user profile
    user_profile = profile_serializer.object
    if not user_profile:
        # delete user
        user.delete()
        # failure
        response_data = {'errorCode': 3, 'errorMessage': 'cannot import user profile'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    try:
        user_profile.id = user.profile.id
        user_profile.user = user
        user_profile.save()
    except:
        # delete user
        user.delete()
        # failure
        response_data = {'errorCode': 4, 'errorMessage': 'cannot save user profile'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    # generate a token for the newly created user
    token, created = Token.objects.get_or_create(user=user)

    # success
    response_data = {'errorCode': 0, "errorMessage": ""}
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def delete_user(request):
    # get user_id
    if not 'user_id' in request.DATA:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "POST user_id not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    user_id = request.DATA['user_id']
    # get the user
    try:
        user = User.objects.get(pk=user_id)
        profile = user.profile
        if profile:
            profile.delete()
        user.delete()
    except:
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "cannot find user with pk: %d" % user_id}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    # success
    response_data = {'errorCode': 0, 'errorMessage': ''}
    return HttpResponse(json.dumps(response_data), content_type="application/json")