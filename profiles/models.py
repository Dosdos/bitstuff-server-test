import random
import string

from django.contrib.auth.models import User
from django.db import models


def generate_random_string(size=4, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class ForgotPasswordMixin(models.Model):
    password_forgot_code = models.CharField(max_length=4, default=generate_random_string)

    class Meta:
        abstract = True


class UserProfile(ForgotPasswordMixin):
    user = models.OneToOneField(User, related_name='profile')

    ghost = models.BooleanField(default=False)
    hunter = models.BooleanField(default=False)

    born = models.CharField(max_length=20, null=True, blank=True)
    city = models.CharField(max_length=200, null=True, blank=True)
    sex = models.CharField(max_length=2, null=True, blank=True)

    facebook_id = models.CharField(max_length=100, null=True, blank=True)
    google_plus_id = models.CharField(max_length=100, null=True, blank=True)

    facebook_access_token = models.CharField(max_length=300, null=True, blank=True)

    def __unicode__(self):
        return self.user.username or "User"

    @property
    def email(self):
        return self.user.email

    @property
    def name(self):
        if self.user and self.user.first_name and self.user.last_name:
            return "%s %s" % (self.user.first_name, self.user.last_name)
        elif self.user:
            return "%s" % self.user.username
        else:
            return "Unknown User"