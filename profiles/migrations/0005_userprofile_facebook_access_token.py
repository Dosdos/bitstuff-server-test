# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0004_userprofile_password_forgot_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='facebook_access_token',
            field=models.CharField(max_length=300, null=True, blank=True),
            preserve_default=True,
        ),
    ]
