import json
import uuid
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
import facebook
from django.http.response import HttpResponse
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from fb_auth.serializers import FacebookUserProfileSerializer
from profiles.models import UserProfile


@api_view(['POST'])
@permission_classes((AllowAny,))
def login_fb(request):

    # check if facebook_access_token in request data
    if not request.DATA['facebook_access_token']:
        response_data = json.dumps({
            'errorCode': 1,
            "errorMessage": "error: 'facebook_access_token' not found in POST data",
        })
        return HttpResponse(response_data, content_type="application/json")

    # get fb access token and retrieve Facebook Data for the user
    fb_access_token = request.DATA['facebook_access_token']
    graph = facebook.GraphAPI(fb_access_token)
    fb_user_data = graph.get_object("me")

    # check if user is in db
    if not UserProfile.objects.filter(facebook_id=fb_user_data['id']).count() == 1:
        response_data = json.dumps({
            'errorCode': 2,
            "errorMessage": "user with fb_id %s not found" % fb_user_data['id'],
        })
        return HttpResponse(response_data, content_type="application/json")

    user_profile = UserProfile.objects.filter(facebook_id=fb_user_data['id'])[0]
    user = user_profile.user

    # change user password
    r_password = User.objects.make_random_password()
    user.set_password(r_password)
    user.save()

    # update user data only if access token changed
    if user_profile.facebook_access_token != fb_access_token:
        user.first_name = fb_user_data['first_name']
        user.last_name = fb_user_data['last_name']
        if 'email' in fb_user_data:
            user.email = fb_user_data['email']
        user.save()
        user_profile.born = fb_user_data['birthday']
        user_profile.city = fb_user_data['hometown']['name']
        user_profile.sex = 'm' if fb_user_data['gender'] == 'male' else 'f'
        user_profile.facebook_id = fb_user_data['id']
        user_profile.facebook_access_token = fb_access_token
        user_profile.save()

    # success
    response_data = json.dumps({
        'errorCode': 0,
        "errorMessage": "",
        "username": user.username,
        "password": r_password,
    })

    return HttpResponse(response_data, content_type="application/json")


@api_view(['POST', 'PUT', 'PATCH'])
@permission_classes((IsAuthenticated,))
def user_register_fb(request):
    profile_serializer = FacebookUserProfileSerializer(data=request.DATA)

    if not profile_serializer.is_valid():
        # failure
        response_data = {'errorCode': 2, 'errorMessage': "profile_serializer errors: %s" % profile_serializer.errors}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    # get fb access token and retrieve Facebook Data for the user
    fb_profile = profile_serializer.object
    fb_access_token = fb_profile.facebook_access_token
    graph = facebook.GraphAPI(fb_access_token)
    fb_user_data = graph.get_object("me")

    # check if user already registered with facebook, if positive...
    if UserProfile.objects.filter(facebook_id=fb_user_data['id']) and UserProfile.objects.filter(facebook_id=fb_user_data['id']).count() == 1:
        user_profile = UserProfile.objects.filter(facebook_id=fb_user_data['id'])[0]
        user = user_profile.user

        # change user password
        r_password = User.objects.make_random_password()
        user.set_password(r_password)
        user.save()

        token = get_object_or_404(Token, user=user)

        # update user data only if access token changed
        if user_profile.facebook_access_token != fb_access_token:
            user.first_name = fb_user_data['first_name']
            user.last_name = fb_user_data['last_name']
            if 'email' in fb_user_data:
                user.email = fb_user_data['email']
            user.save()
            user_profile.born = fb_user_data['birthday']
            user_profile.city = fb_user_data['hometown']['name']
            user_profile.sex = 'm' if fb_user_data['gender'] == 'male' else 'f'
            user_profile.facebook_id = fb_user_data['id']
            user_profile.facebook_access_token = fb_access_token
            user_profile.save()

        response_data = json.dumps({
            'errorCode': 0,
            "errorMessage": "",
            "username": user.username,
            "password": r_password,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "ghost": user_profile.ghost,
            "hunter": user_profile.hunter,
            "born": user_profile.born,
            "city": user_profile.city,
            "sex": user_profile.sex,
            "facebook_id": user_profile.facebook_id,
            "google_plus_id": user_profile.google_plus_id,
            "facebook_access_token": user_profile.facebook_access_token,
            "token": token.key,
        })
        return HttpResponse(response_data, content_type="application/json")

    # ... if not create a user
    # get user with token
    user = request.user

    # update user info
    user.first_name = fb_user_data['first_name']
    user.last_name = fb_user_data['last_name']
    user.username = fb_user_data['id']
    # add chunk of uuid if username already in use
    if User.objects.filter(username=user.username):
        user.username += str(uuid.uuid4())[:8]
    if 'email' in fb_user_data:
        user.email = fb_user_data['email']
    r_password = User.objects.make_random_password()
    user.set_password(r_password)
    user.save()

    # update user profile
    user_profile = profile_serializer.object
    if not user_profile:
        # delete user
        user.delete()
        # failure
        response_data = {'errorCode': 3, 'errorMessage': 'cannot import user profile'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    try:

        user_profile.id = user.profile.id
        user_profile.user = user
        user_profile.born = fb_user_data['birthday']
        user_profile.city = fb_user_data['hometown']['name']
        user_profile.sex = 'm' if fb_user_data['gender'] == 'male' else 'f'
        user_profile.facebook_id = fb_user_data['id']
        user_profile.facebook_access_token = fb_access_token  # save in db facebook access token as well
        user_profile.save()

    except:
        # delete user
        user.delete()
        # failure
        response_data = {
            'errorCode': 4,
            'errorMessage': 'cannot save user profile',
            'fb_access_token': fb_access_token,

        }
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    # generate a token for the newly created user
    token, created = Token.objects.get_or_create(user=user)

    # success
    response_data = json.dumps({
        'errorCode': 0,
        "errorMessage": "",
        "username": user.username,
        "password": r_password,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "ghost": user_profile.ghost,
        "hunter": user_profile.hunter,
        "born": user_profile.born,
        "city": user_profile.city,
        "sex": user_profile.sex,
        "facebook_id": user_profile.facebook_id,
        "google_plus_id": user_profile.google_plus_id,
        "facebook_access_token": user_profile.facebook_access_token,
        "token": token.key,
    })
    return HttpResponse(response_data, content_type="application/json")