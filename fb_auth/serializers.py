from django.contrib.auth.models import User
from rest_framework import serializers
from profiles.models import UserProfile


class FacebookUserRegisterSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=100)

    class Meta:
        model = User
        fields = ('password', )


class FacebookUserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = ('ghost', 'hunter', 'facebook_access_token', )