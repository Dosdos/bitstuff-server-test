import random
import string
import sys

from django.db import models
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from push_notifications.models import APNSDevice


class BluetoothMixin(models.Model):
    major = models.IntegerField()
    minor = models.IntegerField()

    class Meta:
        abstract = True
        unique_together = ('major', 'minor')


class CouponMixin(models.Model):
    coupon = models.ForeignKey('coupons.Coupon', blank=True, null=True)

    class Meta:
        abstract = True


class PrivilegesMixin(models.Model):
    owners = models.ManyToManyField('profiles.UserProfile', blank=True, null=True, related_name='peripherals')

    class Meta:
        abstract = True


def generate_random_string(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_random_activation_code():
    while 1:
        code = '{0:09}'.format(random.randint(1, 1000000000))
        try:
            Peripheral.objects.get(activation_key=code)
        except:
            return code


class Peripheral(BluetoothMixin, CouponMixin, PrivilegesMixin):
    name = models.CharField(max_length=50, default=generate_random_string)
    description = models.CharField(max_length=200, blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    activation_code = models.CharField(max_length=9, default=generate_random_activation_code)

    def __unicode__(self):
        return "%s" % self.name

    @property
    def is_lost(self):
        return LostAndFound.objects.filter(enabled=True, peripheral=self).count() > 0


class LostAndFound(models.Model):
    # lost phase
    peripheral = models.ForeignKey(Peripheral)
    enabled = models.BooleanField(default=True)
    anonymous = models.BooleanField(default=False)
    lost_owner = models.ForeignKey('profiles.UserProfile', related_name='lost', null=True, blank=True)
    lost_date = models.DateTimeField(auto_now_add=True)
    lost_where = models.CharField(max_length=100, blank=True, null=True)
    lost_lat = models.FloatField(default=0, blank=True, null=True)
    lost_lon = models.FloatField(default=0, blank=True, null=True)
    lost_notes = models.CharField(max_length=200, blank=True, null=True)
    # found phase
    found = models.BooleanField(default=False)
    finder = models.ForeignKey('profiles.UserProfile', related_name='lost_finder', null=True, blank=True)
    find_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        if self.anonymous:
            return "Anonymous %s" % self.peripheral
        else:
            return "%s" % self.peripheral


class SecurityArea(models.Model):
    user = models.ForeignKey('profiles.UserProfile', related_name='security_areas')
    peripheral = models.ForeignKey(Peripheral, related_name='areas')
    name = models.CharField(max_length=20)
    enabled = models.BooleanField(default=True)
    lat = models.FloatField(default=0)
    lon = models.FloatField(default=0)
    where = models.CharField(max_length=100, blank=True, null=True)
    radius = models.FloatField(default=1)

    def __unicode__(self):
        return self.name


@receiver(post_save, sender=LostAndFound)
def post_save_lost_found(sender, instance, **kwargs):
    if instance.found and instance.lost_owner:
        userprofile = instance.lost_owner
        user = userprofile.user
        user_devices = userprofile.user_devices.all()
        for user_device in user_devices:
            if user_device.device_unique_id and user_device.push_token:
                try:
                    device = APNSDevice.objects.get(registration_id=user_device.push_token)
                except:
                    try:
                        # apns device not found, create it
                        device = APNSDevice(user=user, device_id=user_device.device_unique_id,
                                            registration_id=user_device.push_token)
                        device.save()
                    except:
                        print "error: {0} - {1}".format(sys.exc_info()[0], sys.exc_info()[1])
                if device:
                    device.send_message("il tuo bit e' stato ritrovato")  # Alert message may only be sent as text.
                    print "push notification sent"
