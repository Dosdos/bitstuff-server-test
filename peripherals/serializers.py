from import_export import resources
from rest_framework import serializers

from peripherals.models import Peripheral, LostAndFound, SecurityArea
from profiles.models import UserProfile
from userdevices.serializers import UserDeviceSerializer


class PeripheralResource(resources.ModelResource):
    class Meta:
        model = Peripheral


class PeripheralCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Peripheral


class PeripheralSerializer(serializers.ModelSerializer):
    is_lost = serializers.BooleanField(source='is_lost')

    class Meta:
        model = Peripheral
        fields = ('id', 'major', 'minor', 'coupon', 'name', 'description', 'creation_date',
                  'activation_code', 'owners', 'is_lost')


class LostAndFoundCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = LostAndFound


class LostAndFoundSerializer(serializers.ModelSerializer):
    peripheral_name = serializers.CharField(source='peripheral.name', blank=True)
    lost_owner_name = serializers.CharField(source='lost_owner.name', blank=True)

    class Meta:
        model = LostAndFound


class SecurityAreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = SecurityArea


class PermissionsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='id')
    first_name = serializers.CharField(source='user.first_name')
    last_name = serializers.CharField(source='user.last_name')
    username = serializers.CharField(source='user.username')
    email = serializers.CharField(source='user.email')
    #
    peripherals = PeripheralSerializer(many=True)
    losts = LostAndFoundSerializer(many=True, source='mylost')
    founds = LostAndFoundSerializer(many=True, source='lost_finder')
    userdevices = UserDeviceSerializer(many=True, source='user_devices')
    areas = SecurityAreaSerializer(many=True, source='security_areas')

    class Meta:
        model = UserProfile
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'ghost', 'hunter',
                  'born', 'city', 'sex', 'facebook_id', 'google_plus_id',
                  'peripherals', 'losts', 'founds', 'userdevices', 'areas')