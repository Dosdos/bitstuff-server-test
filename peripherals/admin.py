import sys

from django.contrib import admin, messages
from django.contrib.admin import helpers
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _
from import_export.admin import ImportExportModelAdmin

from peripherals.forms import AdminBulkCreateForm
from peripherals.models import Peripheral, LostAndFound, SecurityArea
from peripherals.serializers import PeripheralResource


class PeripheralAdmin(ImportExportModelAdmin):
    resource_class = PeripheralResource
    actions = ['create_bulk_peripherals']
    list_display = ['pk', 'name', 'activation_code', 'creation_date', 'major', 'minor']
    list_filter = ['major']
    filter_vertical = ['owners']
    search_fields = ['minor']

    # fix for empty selection
    def changelist_view(self, request, extra_context=None):
        post = request.POST.copy()
        if helpers.ACTION_CHECKBOX_NAME not in post:
            post.update({helpers.ACTION_CHECKBOX_NAME: None})
            request._set_post(post)
        return super(PeripheralAdmin, self).changelist_view(request, extra_context)


    def create_bulk_peripherals(self, request, queryset, *args, **kwargs):
        form = None

        if 'lotto' in request.POST:
            form = AdminBulkCreateForm(request.POST)
            if form.is_valid():
                lotto = form.cleaned_data['lotto']
                from_minor = form.cleaned_data['from_minor']
                to_minor = form.cleaned_data['to_minor']
                # create_coupons = form.cleaned_data['create_coupons']

                peripherals = []
                try:
                    for i in range(from_minor, to_minor):
                        # convert i to hex
                        peripheral = Peripheral(major=lotto, minor=i)
                        peripherals.append(peripheral)

                        # if create_coupons:
                        # coupon = Coupon()
                        # peripheral.coupon = coupon

                    # bulk save
                    Peripheral.objects.bulk_create(peripherals)
                except RuntimeError as e:
                    # manage error
                    self.message_user(request, _("Check form inputs and try again (%s)") % sys.exc_info()[0],
                                      level=messages.ERROR)
                    return render(request, 'peripherals/admin/bulk_create.html', {'form': form, 'queryset': queryset})

                # finish
                self.message_user(request, _("created %d peripherals for product lot %d") % (10, lotto))
                return HttpResponseRedirect(request.get_full_path())
            else:
                # manage invalid form
                return render(request, 'peripherals/admin/bulk_create.html', {'form': form, 'queryset': queryset})

        # initial form
        if not form:
            form = AdminBulkCreateForm()
        return render(request, 'peripherals/admin/bulk_create.html', {'form': form, 'queryset': queryset})

    create_bulk_peripherals.short_description = _("Create new product lot")


class LostAndFoundAdmin(admin.ModelAdmin):
    list_display = ['pk', 'peripheral', 'enabled', 'anonymous', 'lost_owner', 'lost_date', 'lost_where', 'found', 'finder',
                    'find_date']
    list_filter = ['enabled', 'found']


class SecurityAreaAdmin(admin.ModelAdmin):
    list_display = ['pk', 'user', 'peripheral', 'name', 'enabled', 'lat', 'lon', 'radius']
    list_filter = ['enabled']


admin.site.register(Peripheral, PeripheralAdmin)
admin.site.register(LostAndFound, LostAndFoundAdmin)
admin.site.register(SecurityArea, SecurityAreaAdmin)
