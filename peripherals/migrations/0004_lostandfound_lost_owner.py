# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
        ('peripherals', '0003_remove_lostandfound_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='lostandfound',
            name='lost_owner',
            field=models.ForeignKey(related_name=b'lost', blank=True, to='profiles.UserProfile', null=True),
            preserve_default=True,
        ),
    ]
