"""
WSGI config for bitstuff project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application
from bitstuff.settings import ENV

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bitstuff.settings")

if ENV == 'prod':
    from dj_static import Cling
    application = Cling(get_wsgi_application())
else:
    application = get_wsgi_application()