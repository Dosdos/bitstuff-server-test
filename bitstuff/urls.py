import json

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http.response import HttpResponse
from django.utils.translation import ugettext_lazy
from fb_auth.views import user_register_fb, login_fb

from network.views import create_network_email
from peripherals.views import PeripheralsView, PermissionsView, activate_peripheral, LostFoundCreateView, LostFoundView, \
    PeripheralUpdateView, LostFoundListView, SecurityAreasListView, SecurityAreaView, SecurityAreaCreateView, \
    connect_peripheral, disconnect_peripheral, peripheral_patch
from profiles.views import UserProfileView, user_register, delete_user, password_forgot_request, password_forgot_change, \
    password_change
from userdevices.views import manage_userdevice, UserDeviceView, logout, UserDevicePkView


class ExceptionMiddleware(object):
    def process_exception(self, request, exception):
        try:
            response_data = {'errorCode': 1000, 'errorMessage': "server error: %s" % exception.message}
        except:
            response_data = {'errorCode': 1001, 'errorMessage': 'generic server error'}
        return HttpResponse(json.dumps(response_data), content_type="application/json")


urlpatterns = patterns('',

                       # registration with fb token
                       url(r'^profile_fb/', user_register_fb),
                       url(r'^login_fb/', login_fb),

                       # userdevice management
                       url(r'^userdevice', manage_userdevice),
                       url(r'^userdevice/(?P<pk>\d+)/$', UserDevicePkView.as_view()),
                       url(r'^userdevice/(?P<device_unique_id>\w+)/$', UserDeviceView.as_view()),

                       # user management
                       url(r'^user', UserProfileView.as_view()),
                       url(r'^profile', user_register),
                       url(r'^profile/(?P<pk>\d+)/$', user_register),
                       url(r'^delete_user', delete_user),
                       url(r'^logout', logout),

                       # password management
                       url(r'^pwd_forgot_request', password_forgot_request),
                       url(r'^pwd_forgot_change', password_forgot_change),
                       url(r'^pwd_change', password_change),

                       # peripherals management
                       url(r'^peripherals', PeripheralsView.as_view()),
                       url(r'^peripheral/(?P<pk>\d+)/$', PeripheralUpdateView.as_view()),
                       url(r'^peripheral_patch', peripheral_patch),
                       url(r'^permissions', PermissionsView.as_view()),
                       url(r'^activate', activate_peripheral),
                       url(r'^connect', connect_peripheral),
                       url(r'^disconnect', disconnect_peripheral),

                       # lost and found management
                       url(r'^losts', LostFoundListView.as_view()),
                       url(r'^lost/$', LostFoundCreateView.as_view()),
                       url(r'^lost/(?P<pk>\d+)/$', LostFoundView.as_view()),

                       # areas
                       url(r'^areas', SecurityAreasListView.as_view()),
                       url(r'^area/$', SecurityAreaCreateView.as_view()),
                       url(r'^area/(?P<pk>\d+)/$', SecurityAreaView.as_view()),

                       # network and mails
                       url(r'^emails', create_network_email),

                       # admin and api auth
                       # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
                       url(r'^api-token-auth/', 'rest_framework.authtoken.views.obtain_auth_token'),
                       url(r'^admin/', include(admin.site.urls)),
)

admin.site.site_title = ugettext_lazy('BitStuff site admin')

# Text to put in each page's <h1>.
admin.site.site_header = ugettext_lazy('BitStuff administration')

# Text to put at the top of the admin index page.
admin.site.index_title = ugettext_lazy('Site administration')