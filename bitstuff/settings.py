"""
Django settings for bitstuff project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'p=sfkd8lykv^wr8z^o&y_44=m(*p&(ju2t39v01+5dm*ol&8#z'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# ENV = 'Development'
# ENV = 'test'
ENV = 'prod'

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'debug_toolbar.apps.DebugToolbarConfig',

    'import_export',
    'sekizai',

    'rest_framework',
    'rest_framework.authtoken',

    'profiles',
    'userdevices',
    'coupons',
    'peripherals',
    'network',

    'push_notifications'
)

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.AllowAny',
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    )
}

MIDDLEWARE_CLASSES = (
    'bitstuff.urls.ExceptionMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'bitstuff.backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend'
)

PUSH_NOTIFICATIONS_SETTINGS = {
    "GCM_API_KEY": "<your api key>",
    "APNS_CERTIFICATE": os.path.join(PROJECT_PATH, "bitstuff/pem/production_no_key.pem"),
    #"APNS_CERTIFICATE": os.path.join(PROJECT_PATH, "bitstuff/pem/development_no_key.pem"),
    #"APNS_HOST": "gateway.sandbox.push.apple.com",
    "APNS_HOST": "gateway.push.apple.com"
}

ROOT_URLCONF = 'bitstuff.urls'

WSGI_APPLICATION = 'bitstuff.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
    #     'ENGINE': 'django.db.backends.postgresql_psycopg2',
    #     'NAME': 'bitstuff',
    #     'USER': 'postgres',
    #     'PASSWORD': 'postgres',
    #     'HOST': '127.0.0.1',
    #     'PORT': '5432',
    # },
    # 'default2': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'mydatabase'
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = "staticfiles"

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

# email settings
DEFAULT_FROM_EMAIL = 'daniele@toodev.com'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'daniele@toodev.com'
EMAIL_HOST_PASSWORD = 'dani345**'

# Loading test/prod settings based on ENV settings
if ENV == 'prod':
    try:
        from prod_settings import *
    except ImportError:
        pass