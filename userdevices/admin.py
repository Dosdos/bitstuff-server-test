from django.contrib import admin
from userdevices.models import UserDevice


class UserDeviceAdmin(admin.ModelAdmin):
    list_display = ['pk', 'device_unique_id', 'push_token', 'user_list', 'model', 'os', 'os_version']
    filter_vertical = ['users']
    list_filter = ['model', 'os', 'os_version']
    search_fields = ['user', 'device_unique_id']


admin.site.register(UserDevice, UserDeviceAdmin)