from django.contrib.auth.models import User, AnonymousUser
from django.db import models


class UserDevice(models.Model):

    users = models.ManyToManyField('profiles.UserProfile', related_name='user_devices', blank=True, null=True)

    enabled = models.BooleanField(default=True)
    device_unique_id = models.CharField(max_length=33, unique=True)
    #
    push_token = models.CharField(max_length=100, blank=True, null=True)
    #
    label = models.CharField(max_length=50, blank=True, null=True)
    brand = models.CharField(max_length=20, blank=True, null=True)
    model = models.CharField(max_length=20, blank=True, null=True)
    os = models.CharField(max_length=20, blank=True, null=True)
    os_version = models.CharField(max_length=10, blank=True, null=True)

    def __unicode__(self):
        return "%s" % self.device_unique_id


    @property
    def user_list(self):
        return "\n".join([("%s" % e) for e in self.users.all()])