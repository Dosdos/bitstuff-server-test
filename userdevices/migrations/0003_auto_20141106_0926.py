# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('userdevices', '0002_auto_20140929_1024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userdevice',
            name='label',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
