# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserDevice',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('enabled', models.BooleanField(default=True)),
                ('device_unique_id', models.CharField(unique=True, max_length=32)),
                ('push_token', models.CharField(max_length=100, null=True, blank=True)),
                ('label', models.CharField(max_length=20, null=True, blank=True)),
                ('brand', models.CharField(max_length=20, null=True, blank=True)),
                ('model', models.CharField(max_length=20, null=True, blank=True)),
                ('os', models.CharField(max_length=20, null=True, blank=True)),
                ('os_version', models.CharField(max_length=10, null=True, blank=True)),
                ('users', models.ManyToManyField(related_name=b'user_devices', null=True, to='profiles.UserProfile', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
