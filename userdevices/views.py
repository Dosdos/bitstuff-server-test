import json
import sys

from django.contrib.auth.models import User
from django.core.exceptions import FieldError
from django.http.response import HttpResponse
from rest_framework import generics
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import authentication_classes, permission_classes, api_view
from rest_framework.permissions import AllowAny, IsAuthenticated

from profiles.models import UserProfile
from userdevices.models import UserDevice
from userdevices.serializers import UserDeviceSerializer


@api_view(['POST'])
@permission_classes((AllowAny,))
def manage_userdevice(request):
    #
    serializer = UserDeviceSerializer(data=request.DATA)
    # get device_unique_id
    try:
        device_unique_id = serializer.init_data['deviceuniqueid']
        id_len = len(device_unique_id)
        if 32 != id_len:
            # failure
            response_data = {'errorCode': 2, 'errorMessage': "deviceuniqueid not 32 chars"}
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    except:
        print "error: {0} - {1}".format(sys.exc_info()[0], sys.exc_info()[1])
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "deviceuniqueid not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    # the token does not exists, therefore the user does not exist either
    # check if userdevice exists
    userdevice = None
    userdevices = UserDevice.objects.filter(device_unique_id=device_unique_id)
    userdevices_size = len(userdevices)
    if userdevices and userdevices_size == 1:
        userdevice = userdevices[0]

    # manage USERDEVICE
    # update model if found
    if userdevice:
        try:
            userdevice.label = serializer.init_data['label']
            userdevice.brand = serializer.init_data['marca']
            userdevice.model = serializer.init_data['modello']
            userdevice.os = serializer.init_data['os']
            userdevice.os_version = serializer.init_data['os_version']
            userdevice.save()
            # success
        except:
            mex = "error: {0} - {1}".format(sys.exc_info()[0], sys.exc_info()[1])
            # failure
            response_data = {'errorCode': 5, 'errorMessage': "error while updating userdevice: {0}".format(mex)}
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        # create the userdevice
        # if the model does not exists and the serializer is valid, userdevice can be saved
        if serializer.is_valid():
            # create model
            userdevice = serializer.save(force_insert=True)
        else:
            # failure
            response_data = {'errorCode': 4,
                             'errorMessage': "user device invalid with serializer errors: %s" % serializer.errors}
            return HttpResponse(json.dumps(response_data), content_type="application/json")

    # manage GHOST USER
    # check if user already exists associated to the userdevice received
    # if so, return the user token
    # check if there's a user associated with the newly created userdevice
    users = User.objects.filter(profile__ghost=True, profile__user_devices=userdevice)
    if not users:
        # create temporary user associated with this userdevice
        # user information will be empty for now
        username = userdevice.device_unique_id[2:]
        try:
            email = ""
            password = ""
            user = User.objects.create_user(username, email, password)
            # create profile for user
            profile = UserProfile(user=user)
            profile.ghost = True
            profile.save()
            # associate user device to user
            userdevice.users.add(profile)
            userdevice.save()
            try:
                # generate token for user
                token, created = Token.objects.get_or_create(user=user)
                # success
                response_data = {'token': token.key}
            except:
                # failure
                response_data = {'errorCode': 1, 'errorMessage': "token generation failed"}
        except:
            # failure
            response_data = {'errorCode': 2,
                             'errorMessage': (
                                 "user device invalid: {0} - {1}".format(sys.exc_info()[0], sys.exc_info()[1]))}
    elif users.count() > 1:
        # multiple ghost user found, this is an error!
        response_data = {'errorCode': 3, 'errorMessage': "user not found"}
    else:
        # ghost user already present, return its token
        user = users[0]
        token, created = Token.objects.get_or_create(user=user)
        # success
        response_data = {'token': token.key}

    # response json
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@permission_classes((AllowAny,))
class UserDevicePkView(generics.RetrieveUpdateDestroyAPIView):
    model = UserDevice
    serializer_class = UserDeviceSerializer


@permission_classes((AllowAny,))
class UserDeviceView(generics.RetrieveUpdateDestroyAPIView):
    model = UserDevice
    serializer_class = UserDeviceSerializer
    lookup_field = 'device_unique_id'


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def logout(request):
    #
    serializer = UserDeviceSerializer(data=request.DATA)
    # get device_unique_id
    try:
        device_unique_id = serializer.init_data['deviceuniqueid']
        id_len = len(device_unique_id)
        if 32 != id_len:
            # failure
            response_data = {'errorCode': 2, 'errorMessage': "deviceuniqueid not 32 chars"}
            return HttpResponse(json.dumps(response_data), content_type="application/json")
    except:
        print "error: {0} - {1}".format(sys.exc_info()[0], sys.exc_info()[1])
        # failure
        response_data = {'errorCode': 1, 'errorMessage': "deviceuniqueid not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    # the token does not exists, therefore the user does not exist either
    # check if userdevice exists
    try:
        userdevice = UserDevice.objects.get(device_unique_id=device_unique_id)
    except:
        print "error: {0} - {1}".format(sys.exc_info()[0], sys.exc_info()[1])
        # failure
        response_data = {'errorCode': 3, 'errorMessage': "userdevice not found"}
        return HttpResponse(json.dumps(response_data), content_type="application/json")
        pass
    # remove the user from the userdevice relationship
    user = request.user
    userdevice.users.remove(user.profile)
    userdevice.save()
    # success
    response_data = {'errorCode': 0, 'errorMessage': ""}
    return HttpResponse(json.dumps(response_data), content_type="application/json")