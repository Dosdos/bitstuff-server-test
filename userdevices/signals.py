from django.contrib.auth.models import User

from profiles.models import UserProfile



# @receiver(post_save, sender=UserDevice)
def post_save_userdevice(sender, **kwargs):
    userdevice = kwargs['instance']
    if not kwargs['created']:
        return
    # check if there's a user associated with the newly created userdevice
    user = User.objects.filter(profile__user_devices=userdevice)
    if not user:
        # create temporary user associated with this userdevice
        username = userdevice.device_unique_id
        try:
            email = ""
            password = ""
            user = User.objects.create_user(username, email, password)
            # create profile for user
            profile = UserProfile(user=user)
            profile.save()
            # associate user device to user
            userdevice.users.add(profile)
            userdevice.save()
            #
            all = userdevice.users.all()
            print "associated profiles to user device: %s" % all
        except:
            pass
