from django.core.serializers import json
from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated

from coupons.models import Coupon


class DeviceActivationViewSet(viewsets.ModelViewSet):
    # authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    model = Coupon


"""
class ActivateDeviceAPIView(APIView):
    authentication_classes = (TokenAuthentication,)

    def post(self, request, format=None):
        activation_key = request.DATA['activation_code']
        if activation_key:
            activation = DeviceActivation.objects.get(activation_key=activation_key)
            if activation:
                activation.activated = True
                activation.save()
                return Response(request.DATA, status=status.HTTP_201_CREATED)
        return Response(None, status=status.HTTP_400_BAD_REQUEST)
"""


@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
def activate_device(request, pk):
    response_data = {}
    try:
        activation = Coupon.objects.get(activation_key=pk)
        if activation:
            activation.activated = True
            activation.save()
            response_data['result'] = '1'
    except Coupon.DoesNotExist:
        response_data['result'] = '0'
    return HttpResponse(json.dumps(response_data), content_type="application/json")