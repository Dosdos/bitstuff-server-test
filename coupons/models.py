import random

from django.db import models
from django.utils.translation import ugettext_lazy as _


def coupon_code_generate():
        while 1:
            prom_code = '{0:09}'.format(random.randint(1, 1000000000))
            try:
                Coupon.objects.get(activation_key=prom_code)
            except:
                return prom_code


class Coupon(models.Model):
    activation_key = models.CharField(max_length=9, default=coupon_code_generate, unique=True,
                                      verbose_name=_('activation key'))

    # generation
    creation_date = models.DateField(auto_now_add=True)

    # activation
    activated = models.BooleanField(default=False)
    activation_date = models.DateField(auto_now=True)


    class Meta:
        verbose_name = _('Coupon')
        verbose_name_plural = _('Coupons')

    def __unicode__(self):
        return "%s" % self.activation_key