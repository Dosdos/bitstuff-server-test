from import_export import resources
from rest_framework import serializers

from coupons.models import Coupon


class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon


class CouponResource(resources.ModelResource):
    class Meta:
        model = Coupon